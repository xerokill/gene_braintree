// add support for GoMage LightCheckout extension
vZero.addMethods({
    addGoMageLightCheckoutSupport: function(klass, checkout, checkoutForm, payment) {
        var vzero = this,
            prototype = klass.prototype;

        // check if custom code has already been injected
        if (prototype._geneBraintree) {
            return;
        } else {
            prototype._geneBraintree = true;
        }


        klass.addMethods({
            LightcheckoutSubmit: prototype.LightcheckoutSubmit.wrap(function(originalFn) {
                var me = this,
                    deviceData = $('device_data'),
                    params = {},
                    billingAddress;

                if ((payment.currentMethod == 'gene_braintree_creditcard') && checkoutForm.validator.validate()) {
                    // display loading indicator
                    me.showLoadinfo();

                    // If the billing address is selected, and we want to ship to that address,
                    // then we need to pass the addressId
                    billingAddress = $('billing-address-select');
                    if (billingAddress) {
                        params.addressId = billingAddress.value;
                    }

                    vzero.updateData(function() {
                        // Process the card
                        vzero.process({
                            onSuccess: function() {
                                var ccNumber = $$('[data-genebraintree-name=number]').first();

                                // Always attempt to update the card type on submission
                                if (ccNumber) {
                                    vzero.updateCardType(ccNumber.value);
                                }

                                // disable PayPal nonce
                                $('paypal-payment-nonce').setAttribute('disabled', 'disabled');

                                // ensure device data is always sent with request
                                deviceData.removeAttribute('disabled');

                                originalFn();
                            },

                            onFailure: function() {
                                me.hideLoadinfo();
                            }
                        });
                    }, params);
                } else {
                    originalFn();
                }
            })
        });


        // What should happen if the user closes the 3D secure window?
        vzero.close3dSecureMethod(function() {
            // Re-tokenize all the saved cards
            vzero.tokenize3dSavedCards(checkout.hideLoadInfo);
        });
    }
});


vZeroPayPalButton.addMethods({
    addGoMageLightCheckoutSupport: function(klass, checkout, checkoutForm, payment, completeCheckoutWithText) {
        var vzeroPaypal = this,
            prototype = klass.prototype;

        // check if custom code has already been injected
        if (prototype._geneBraintree) {
            return;
        } else {
            prototype._geneBraintree = true;
        }

        // override paymentForm#switchMethod() to toggle display of paypal button
        klass.addMethods({
            switchMethod: prototype.switchMethod.wrap(function(originalFn, method) {
                var savedAccounts = $('paypal-saved-accounts'),
                    selectedAccount = savedAccounts ? savedAccounts.select('input:checked[type=radio]').first() : null;

                if (
                    (method == 'gene_braintree_paypal') &&
                    (!savedAccounts || (selectedAccount && (selectedAccount.value == 'other')))
                ) {
                    vzeroPaypal.showButton();
                } else {
                    vzeroPaypal.hideButton();
                }

                // toggle display of paypal button when switching between vaulted paypal accounts
                // (only show button only when "other" is selected)
                if (savedAccounts) {
                    savedAccounts.on('click', 'input[type=radio]', function(ev, el) {
                        if (el.value == 'other') {
                            vzeroPaypal.showButton();
                        } else {
                            vzeroPaypal.hideButton();
                        }
                    });
                }

                originalFn(method);
            })
        });

        // add paypal button container
        $('checkout-review-submit').insert([
            '<div id="paypal-complete" style="display:none;">',
                '<div id="paypal-container"></div>',
                '<label id="paypal-label">', completeCheckoutWithText, '</label>',
            '</div>'
        ].join(''));

        // cache button references
        vzeroPaypal.btnPayPal = $('paypal-complete');
        vzeroPaypal.btnSubmit = $('submit-btn');

        // add paypal button
        vzeroPaypal.addPayPalButton({
            onSuccess: function(json) {
                $('paypal-payment-nonce').value = json.nonce;

                // disable credit card nonce
                $('creditcard-payment-nonce').setAttribute('disabled', 'disabled');

                // ensure device data is always sent with request
                $('device_data').removeAttribute('disabled');

                // submit the checkout form
                vzeroPaypal.btnSubmit.click();

                vzeroPaypal.hideButton();
            }
        });

        // prevent paypal login if checkout form is invalid
        $('braintree-paypal-button').on('click', function(ev) {
            if (!checkoutForm.validator.validate()) {
                Event.stop(ev);
            }

            // checkoutForm.validator.validate() automatically disables checkout submit button,
            // so re-enable it again
            vzeroPaypal.btnSubmit.removeClassName('disabled').removeAttribute('disabled');
        });

        // check currently selected payment method on page load
        payment.switchMethod(payment.currentMethod);
    },

    showButton: function() {
        var paypal = this.btnPayPal,
            submit = this.btnSubmit;

        if (paypal) {
            submit.hide();

            // ensure vzeroPaypal is updated with latest quote info
            vzero.updateData(function() {
                paypal.show();
            });
        }
    },

    hideButton: function() {
        var paypal = this.btnPayPal;

        if (paypal) {
            paypal.hide();

            this.btnSubmit.show();
        }
    }
});
