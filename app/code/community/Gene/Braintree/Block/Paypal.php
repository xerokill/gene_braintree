<?php

/**
 * Class Gene_Braintree_Block_Form
 *
 * @author Dave Macaulay <dave@gene.co.uk>
 */
class Gene_Braintree_Block_Paypal extends Mage_Payment_Block_Form
{

    /**
     * Store this so we don't load it multiple times
     */
    private $_savedDetails = false;

    /**
     * Internal constructor. Set template
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('gene/braintree/paypal.phtml');
    }

    /**
     * Generate and return a token
     *
     * @return mixed
     */
    public function getClientToken()
    {
        return Mage::getModel('gene_braintree/wrapper_braintree')->init()->generateToken();
    }

    /**
     * Does this customer have saved accounts?
     *
     * @return mixed
     */
    public function hasSavedDetails()
    {
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            if($this->getSavedDetails()) {
                return sizeof($this->getSavedDetails());
            }
        }
        return false;
    }

    /**
     * Return the saved accounts
     *
     * @return bool
     */
    public function getSavedDetails()
    {
        if(!$this->_savedDetails) {
            $this->_savedDetails = Mage::getSingleton('gene_braintree/saved')->getSavedMethodsByType(Gene_Braintree_Model_Saved::SAVED_PAYPAL_ID);
        }
        return $this->_savedDetails;
    }

    /**
     * Is the vault enabled? Meaning we can save PayPal
     *
     * @return mixed
     */
    public function canSavePayPal()
    {
        if ($this->getMethod()->isVaultEnabled()
            && (Mage::getSingleton('customer/session')->isLoggedIn()
                || Mage::getSingleton('checkout/type_onepage')->getCheckoutMethod() == Mage_Checkout_Model_Type_Onepage::METHOD_REGISTER))
        {
            return true;
        }

        return false;
    }

}